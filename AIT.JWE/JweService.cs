﻿using AIT.JWE.Extensions;
using AIT.JWE.Models;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace AIT.JWE;

public interface IJweService
{
    string BuildToken(IJweConfiguration jweConfiguration, ClaimsIdentity claimsIdentity = null);
    IJweValidationResult ValidateToken(IJweConfiguration jweConfiguration, string token);
}

class JweService : IJweService
{
    private readonly ILogger<JweService> _logger;

    public JweService(
        ILogger<JweService> logger
        )
    {
        _logger = logger;
    }

    public string BuildToken(IJweConfiguration jweConfiguration, ClaimsIdentity claimsIdentity = null)
    {
        try
        {
            _logger.LogDebug("Building JWE token");

            if (jweConfiguration == null) throw new ArgumentNullException(nameof(jweConfiguration));

            var handler = new JwtSecurityTokenHandler();

            _logger.LogDebug("Building token descriptor");
            var tokenDescriptor = jweConfiguration.BuildSecurityTokenDescriptor(claimsIdentity);
            _logger.LogDebug("Token descriptor built successfully");

            _logger.LogDebug("Building token");
            var token = handler.CreateEncodedJwt(tokenDescriptor);
            _logger.LogDebug("Token built successfully");

            return token;
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, $"Failed to build token: {ex.Message}");
            throw;
        }
    }

    public IJweValidationResult ValidateToken(
        IJweConfiguration jweConfiguration,
        string token
        )
    {
        try
        {
            _logger.LogDebug($"Validating JWE token starting {token?.Substring(0, 5) ?? "**No token provided**"}...");

            if (jweConfiguration == null) return new JweValidationResult($"Parameter {jweConfiguration} was empty");
            if (string.IsNullOrWhiteSpace(token)) return new JweValidationResult($"Parameter {token} was empty");

            var handler = new JwtSecurityTokenHandler();

            _logger.LogDebug("Building token validation parameters");
            var tokenValidationParameters = jweConfiguration.BuildTokenValidationParameters();
            _logger.LogDebug("Token validation parameters built successfully");

            ClaimsPrincipal claimsPrincipal = null;

            _logger.LogDebug("Validating token");
            try
            {
                claimsPrincipal = handler.ValidateToken(
                            token: token,
                            validationParameters: tokenValidationParameters,
                            validatedToken: out SecurityToken securityToken
                            );
            }
            catch (Exception validationEx)
            {
                return new JweValidationResult(validationEx.Message);
            }
            _logger.LogDebug("Token validated successfully");

            _logger.LogDebug("Retireving token claims");
            var claims = claimsPrincipal.Claims;
            _logger.LogDebug("Token claims retrieved successfully");

            _logger.LogDebug("Token validation complete");

            return new JweValidationResult(claims);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, $"Token validation error: {ex.Message}");
            throw new Exception($"Token validation error: {ex.Message}", ex);
        }
    }
}

