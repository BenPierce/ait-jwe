﻿using System.Security.Claims;

namespace AIT.JWE.Models;

public interface IJweValidationResult
{
    public bool IsValid { get; }
    public string ValidationMessage { get; }
    public IEnumerable<Claim> TokenClaims { get; }
}

class JweValidationResult : IJweValidationResult
{
    public bool IsValid { get; private set; }
    public string ValidationMessage { get; private set; }
    public IEnumerable<Claim> TokenClaims { get; private set; }

    public JweValidationResult(
        string errorMessage
        )
    {
        IsValid = false;
        ValidationMessage = errorMessage;
    }

    public JweValidationResult(
        IEnumerable<Claim> claims
        )
    {
        IsValid = true;
        TokenClaims = claims;
    }
}
