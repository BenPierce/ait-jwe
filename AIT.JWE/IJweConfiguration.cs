﻿namespace AIT.JWE;

public interface IJweConfiguration
{
    string CertificatePfxFilePath { get; }
    string Password { get; }
    string Audience { get; }
    string Issuer { get; }
    DateTime? NotBefore { get; }
    DateTime? IssuedAt { get; }
    DateTime? Expires { get; }

}
