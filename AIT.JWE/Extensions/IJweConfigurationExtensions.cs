﻿using Microsoft.IdentityModel.Tokens;
using System.IO;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;

namespace AIT.JWE.Extensions;

static class IJweConfigurationExtensions
{
    public static SecurityTokenDescriptor BuildSecurityTokenDescriptor(this IJweConfiguration jweConfiguration, ClaimsIdentity claimsIdentity = null)
    {
        return new SecurityTokenDescriptor
        {
            Audience = jweConfiguration.Audience,
            Issuer = jweConfiguration.Issuer,
            NotBefore = jweConfiguration.NotBefore,
            IssuedAt = jweConfiguration.IssuedAt,
            Expires = jweConfiguration.Expires,
            EncryptingCredentials = new X509EncryptingCredentials(jweConfiguration.EncryptingCredentials()),
            Subject = claimsIdentity ?? new ClaimsIdentity()
        };
    }

    public static TokenValidationParameters BuildTokenValidationParameters(this IJweConfiguration jweConfiguration)
    {
        return new TokenValidationParameters
        {
            ValidAudience = jweConfiguration.Audience,
            ValidateAudience = string.IsNullOrWhiteSpace(jweConfiguration.Audience) == false,
            ValidIssuer = jweConfiguration.Issuer,
            ValidateIssuer = string.IsNullOrWhiteSpace(jweConfiguration.Issuer) == false,
            ValidateLifetime = jweConfiguration.NotBefore != null || jweConfiguration.IssuedAt != null || jweConfiguration.Expires != null,
            TokenDecryptionKey = new X509SecurityKey(jweConfiguration.EncryptingCredentials()),
            RequireSignedTokens = false
        };
    }


    private static X509Certificate2 EncryptingCredentials(this IJweConfiguration jweConfiguration)
    {
        if (jweConfiguration == null) throw new ArgumentNullException(nameof(jweConfiguration));
        if (string.IsNullOrWhiteSpace(jweConfiguration.CertificatePfxFilePath)) throw new ArgumentNullException(nameof(jweConfiguration.CertificatePfxFilePath));
        if (string.IsNullOrWhiteSpace(jweConfiguration.Password)) throw new ArgumentNullException(nameof(jweConfiguration.Password));

        string certicatePfxFilePath = Environment.ExpandEnvironmentVariables(jweConfiguration.CertificatePfxFilePath);

        if (File.Exists(certicatePfxFilePath) == false) throw new FileNotFoundException(certicatePfxFilePath);

        return new X509Certificate2(
            fileName: certicatePfxFilePath,
            password: jweConfiguration.Password
            );
    }
}
