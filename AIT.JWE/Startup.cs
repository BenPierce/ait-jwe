﻿using Microsoft.Extensions.DependencyInjection;

namespace AIT.JWE;

public static class Startup
{
    public static void AddJWE(this IServiceCollection services)
    {
        services.AddTransient<IJweService, JweService>();
    }
}
